const root = document.querySelector( "#root" );
const cityInputField = document.querySelector( "#city-input-field" );
const cityInputButton = document.querySelector( "#city-submit-button" );
const meteoDisplay = document.querySelector( "#meteo-display" );

cityInputButton.addEventListener( "click", function ()
{
    meteoDisplay.innerHTML = "";

    let url = ( "https://api.openweathermap.org/data/2.5/weather?q=" + cityInputField.value + "&appid=b6268c4c0bec38f441539c641c9dfafb" );

    fetch( url )
        .then( content => content.json() )
        .then( elem =>
        {
            console.log(elem);
            if ( elem.ok )
            {
                // Éléments s'affichant une fois le bouton cliqué en allant piocher dans l'API d'OpenWeathers

                descriptionText = element( "p", "description-text", meteoDisplay, "Stats for " + elem.name + " :", "mb-4 text-info" );

                weatherText = element( "p", "weather-text", meteoDisplay, "Weather : " + elem.weather[ 0 ].main + " ( " + elem.weather[ 0 ].description + " )" );

                mainText = element( "p", "main-text", meteoDisplay,
                    "Temperature : " + ( elem.main.temp - 273.15 ).toFixed( 1 ) + "°C"
                    + "<br />" +
                    "Temperature feels like : " + ( elem.main.feels_like - 273.15 ).toFixed( 1 ) + "°C"
                    + "<br />" +
                    "Temperature min : " + ( elem.main.temp_min - 273.15 ).toFixed( 1 ) + "°C"
                    + "<br />" +
                    "Temperature max : " + ( elem.main.temp_max - 273.15 ).toFixed( 1 ) + "°C"
                    + "<br />" +
                    "Pressure : " + elem.main.pressure + " millibars"
                    + "<br />" +
                    "Humidity : " + elem.main.pressure + " millibars"
                    , "mt-2" );

                windText = element( "p", "wind-text", meteoDisplay,
                    "Wind speed : " + elem.wind.speed + " mph"
                    + "<br />" +
                    "Wind degree : " + elem.wind.deg
                    , "mt-2" );

            } else
            {
                errorText = element( "p", "error-text", meteoDisplay, "No city was found for the name : \"" + cityInputField.value + "\"", "text-danger" );
            }
        } )
        .catch( error => console.log( error.message ) );
} );

function element ( tag, id, parent, content, elementClassName = "" )
{
    createdElement = document.createElement( tag );
    createdElement.id = id;
    createdElement.innerHTML = content;
    createdElement.className = elementClassName;
    parent.appendChild( createdElement );
    return createdElement;
}